import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vue-shortkey'
import App from './App.vue'
import router from './router'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.config.productionTip = false

new Vue({
	router,
	render: h => h(App),
	created() {
		// Prevent blank screen in Electron builds
		// this.$router.push('/')
	}
}).$mount('#app')
