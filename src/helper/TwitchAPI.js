import axios from 'axios'
import TwitchJS from "twitch-js";
import Shared from "@/helper/Shared"

export default class TwitchAPI {

	static isChannelLive(name) {
		return new Promise((resolve, reject) => {
			axios.get('https://api.twitch.tv/helix/streams', {
				params: { user_login: name },
				headers: { 'Client-ID': 'z7aj3t08xyf006k5rjb4u25y4t296b' }
			})
				.then(function (response) {
					resolve(response.data)
				})
				.catch(function (error) {
					reject(error)
				})
		})
	}

	static isValidLink(link) {
		return /https:\/\/www.twitch\.tv\/(.*)/.test(link);
	}

	static getChannel(link) {
		return /https:\/\/www.twitch\.tv\/(.*)/.exec(link)[1];
	}

	static initChat(link, messages) {
		try {
			Shared.clearColorCache();

			const options = {
				connection: {
					reconnect: true,
					secure: true
				},
				options: {
					debug: false
				},
				channels: [`#${TwitchAPI.getChannel(link)}`]
			};

			let chatConnection = new TwitchJS.Client(options);

			chatConnection.on(
				"chat",
				(channel, userstate, message, self) => {
					messages.push({
						user: userstate["display-name"],
						message,
						color: Shared.randomColor(userstate["user-id"]),
						id: userstate["id"],
						badges: userstate["badges"] || {}
					});

					if (self) return;
				}
			);

			chatConnection.connect();

			return chatConnection

		} catch (error) {
			throw "Error getting chat"
		}
	}

	static getStatusChannel(link) {
		return new Promise((resolve) => {
			this.isChannelLive(this.getChannel(link)).then(
				data => {
					let isAlive = data.data.length;
					let streamTitle = null;
					let viewers = 0;

					if (isAlive) {
						streamTitle = data.data[0].title;
						viewers = data.data[0].viewer_count;
					}

					resolve([isAlive, streamTitle, viewers])
				}
			);
		});

	}

	static closeConnection(connection) {
		connection.disconnect();
	}
}