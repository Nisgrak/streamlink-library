'use strict'

import { BrowserWindow, shell } from 'electron'
import {
	createProtocol,
} from 'vue-cli-plugin-electron-builder/lib'

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

export default class ChatWindow {

	static createWindow(args) {
		let win
		// Create the browser window.
		win = new BrowserWindow({ width: 800, height: 600, minWidth: 730, minHeight: 300, webPreferences: { webSecurity: false }, frame: false })

		if (process.env.WEBPACK_DEV_SERVER_URL) {
			// Load the url of the dev server if in development mode
			win.loadURL(process.env.WEBPACK_DEV_SERVER_URL + "#/chat")
			if (!process.env.IS_TEST) win.webContents.openDevTools()
		} else {
			createProtocol('app')
			// Load the index.html when not in development
			win.loadURL('app://./index.html#chat')
		}


		win.webContents.on('will-navigate', (event, url) => {
			event.preventDefault()
			shell.openExternal(url)
		});

		win.webContents.on('did-finish-load', () => {
			win.webContents.send('get-data-chat', args)
		})

		win.on('closed', () => {
			win = null
		})
	}
}
