const util = require('util');
const exec = util.promisify(require('child_process').exec)
import Store from "electron-store";
const store = new Store();

export default class StreamlinkPlay {

	static play(link) {
		return exec(`streamlink "${link}" ${store.get('config.qualityStream', 'best')}`)

	}
}