import TwitchAPI from "@/helper/TwitchAPI"
import YoutubeAPI from "@/helper/YoutubeAPI"

export default class Shared {

	static setEmoteParser(emoteParser) {
		this.emoteParser = emoteParser;
	}

	static getEmoteParser() {
		return this.emoteParser;
	}

	static checkTypeLink(link) {
		if (TwitchAPI.isValidLink(link)) {
			return "twitch"
		} else if (YoutubeAPI.isValidLink(link)) {
			return "youtube"
		} else {
			return ""
		}
	}

	static getAPILibWithLink(link) {
		let type = this.checkTypeLink(link)

		return this.getAPILib(type)
	}

	static getAPILib(type) {
		if (type == "twitch") {
			return TwitchAPI;
		} else if (type == "youtube") {
			return YoutubeAPI;
		} else {
			return null;
		}
	}

	static randomColor(id) {
		const r = () => Math.floor(256 * Math.random());

		return (
			this.colorCache[id] ||
			(this.colorCache[id] = `rgb(${r()}, ${r()}, ${r()})`)
		);
	}

	static clearColorCache() {
		this.colorCache = [];
	}
	
	static getDefaultConfig() {
		return {
			showUserBadges: true,
			showTimeInChat: true,
			showNotifications: true,
			openChatAuto: true,
			qualityStream: "Best",
			qualityOptions: ["Best", "Worst"]
		}
	}

	static getUrlRegex() {
		return /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:\/[+~%/.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[\w]*))?)/gi
	}
	
}