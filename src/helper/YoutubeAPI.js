import axios from 'axios'
import YoutubeJS from "youtube-live-chat"
import Shared from "@/helper/Shared"

// AIzaSyDDlZbbU-rx9OeemZtEzxYOSg6B5SXg_CM
export default class YoutubeAPI {

	static isChannelLive(name) {
		return new Promise((resolve, reject) => {
			axios.get('https://www.googleapis.com/youtube/v3/search?', {
				params: { part: "snippet", channelId: name, type: "video", eventType: "live", key: "AIzaSyDDlZbbU-rx9OeemZtEzxYOSg6B5SXg_CM" }
			})
				.then(function (response) {
					resolve(response.data)
				})
				.catch(function (error) {
					reject(error)
				})
		})
	}

	static getViewersOfLive(liveId) {
		return new Promise((resolve, reject) => {
			axios.get('https://www.googleapis.com/youtube/v3/videos?', {
				params: { part: "liveStreamingDetails", id: liveId, key: "AIzaSyDDlZbbU-rx9OeemZtEzxYOSg6B5SXg_CM" }
			})
				.then(function (response) {
					resolve(response.data)
				})
				.catch(function (error) {
					reject(error)
				})
		})
	}

	static isValidLink(link) {
		return /https:\/\/www.youtube\.com\/channel\/(.*)/.test(link);
	}

	static getChannel(link) {
		return /https:\/\/www.youtube\.com\/channel\/(.*)/.exec(link)[1];
	}

	static initChat(link, messages) {
		try {
			Shared.clearColorCache();


			const chatConnection = new YoutubeJS(this.getChannel(link), 'AIzaSyDDlZbbU-rx9OeemZtEzxYOSg6B5SXg_CM');

			chatConnection.on('ready', () => {
				chatConnection.listen(2000)
			})

			chatConnection.on('message', data => {
				messages.push({
					user: data.authorDetails.displayName,
					message: data.snippet.displayMessage,
					color: Shared.randomColor(data.snippet.authorChannelId),
					id: data.id,
					badges: {}
				});
			})

			chatConnection.on('error', error => {
				throw error
			})

			return chatConnection

		} catch (error) {
			throw "Error getting chat"
		}
	}


	static getStatusChannel(link) {
		return new Promise((resolve) => {
			let dataChannel = null;
			this.isChannelLive(this.getChannel(link)).then(
				data => {
					dataChannel = data;
					return this.getViewersOfLive(data.items[0].id.videoId).then(
						dataViews => {
							let isAlive = dataChannel.items.length;
							let streamTitle = null;
							let viewers = 0;

							if (isAlive) {
								streamTitle = dataChannel.items[0].snippet.title.title;
								viewers = dataViews.items[0].liveStreamingDetails.concurrentViewers;
							}

							resolve([isAlive, streamTitle, viewers])
						}
					);
				}
			);
		});

	}

	static closeConnection(connection) {
		connection.stop();
	}
}