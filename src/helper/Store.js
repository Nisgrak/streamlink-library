import fs from 'fs'
import path from 'path'

export default class Store {
	static pathToPublic(name) {

		return path.join(__static, `../public/${name}.json`) // eslint-disable-line no-undef
	}

	static save(name, data) {
		fs.writeFileSync(this.pathToPublic(name), JSON.stringify(data))
	}

	static read(name) {
		if (fs.existsSync(this.pathToPublic(name))) {
			return JSON.parse(fs.readFileSync(this.pathToPublic(name), 'utf8'))

		} else {
			return [];
		}
	}
}