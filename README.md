# streamlink-library

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
yarn electron:serve
```

### Compiles and minifies for production
```
npm run build
yarn electron:build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
