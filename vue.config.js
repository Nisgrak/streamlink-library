module.exports = {
	pluginOptions: {
		electronBuilder: {
			builderOptions: {
				extraResources: ['public/icon.png']
			},
			asar: {
				asarUnpack: './node_modules/node-notifier/vendor/**'
			}
		}
	},
	configureWebpack: {
		devServer: {
			watchOptions: {
				ignored: /public/
			}
		},
		node: {
			__filename: true,
			__dirname: true
		}
	}
}